# hackrf_nexa
## Control your remote power outlet using a HackRF
Code derived from: https://github.com/0xDRRB/hackrf_ook

Information about NEXA protocol collected from: http://tech.jolowe.se/home-automation-rf-protocols/

NOTICE: This program has not yet been tested with a genuine NEXA device, only a device compatible with the NEXA protocol.
If things are not working: adjust the timings to your circumstances!

## Installation
```bash
# Install dependencies (for Debian and derivatives)
sudo apt install cmake hackrf libhackrf-dev libglib2.0-dev

mkdir build
cd build
cmake ..
make

# Create package and install (for Debian and derivatives)
make package
sudo apt install ./hackrf_nexa-0.0.1-Linux.deb

# Install for others
sudo make install
```

## Usage
```bash
hackrf_nexa -h
Usage : hackrf_nexa [OPTIONS]
 -a house_bits        house address associated with your NEXA/Proove/Anslut
 -g group_bit         communicate to a group (?)
 -s state_bit         modify the state of a switch [ON = 0 | OFF = 1]
 -t type_bits         type of switch [NEXA = 11 | Proove/Anslut = 00]
 -u unit_bits         unit to communicate with
 -f hertz             transmit frequency [default: 434000000 Hz]
 -c hertz             carrier frequency [default: 10000 Hz]
 -b us                overall bit duration in microseconds [default: 400µs]
 -r repeat_count      repeat message repeat_count times [default: 6 times]
 -w filename          write IQ data to file and exit
 -h                   show this help
 ```

 Turn the NEXA unit `10` to the state `ON` which is a member of the `01010101010101010101010101` house address:
 ```bash
 hackrf_nexa -a 01010101010101010101010101 -g 1 -s 0 -t 11 -u 10
 ```

Write to IQ file for use with external tools:
```bash
hackrf_nexa -a 01010101010101010101010101 -g 1 -s 0 -t 11 -u 10 -w nexa.cs8

# Analysis with inspectrum
inspectrum nexa.cs8

# Replay with hackrf_transfer
hackrf_transfer -t nexa.cs8 -f 434000000 -s 2000000
```
