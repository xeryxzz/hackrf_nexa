#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <glib.h>
#include <libhackrf/hackrf.h>

char *house_bits;
char *group_bit;
char *state_bit;
char *type_bits;
char *unit_bits;

const uint64_t sample_rate = 2000000;
const unsigned int gain = 47;

uint64_t transmit_freq = 434000000;
uint64_t carrier_freq = 10000;
uint64_t bit_duration = 200 * 2;
int repeat_count = 6;

char *filename = NULL;

int8_t *tx_buffer_I;
int8_t *tx_buffer_Q;
uint64_t message_size;
uint64_t buffer_offset;

const double tau = 2.0 * M_PI;

static hackrf_device *device;

GMainLoop *loop;

int tx_callback(hackrf_transfer *transfer) {
    size_t count = transfer->valid_length;

    for (int i = 0; i < count;) {
        (transfer->buffer)[i++] = tx_buffer_I[buffer_offset];
        (transfer->buffer)[i++] = tx_buffer_Q[buffer_offset];
        buffer_offset++;
        if (repeat_count >= 0 && buffer_offset == message_size) {
            repeat_count--;
            if (repeat_count < 1) {
                g_main_loop_quit(loop);
            }
        }
        buffer_offset %= message_size;
    }
    return 0;
}

size_t char_count(char *str, char compare) {
    size_t count;
    for (count = 0; str[count]; str[count] == compare ? count++ : *str++);
    return count;
}

char *prepare_packet(char *data) {
    size_t ones = char_count(data, '1');
    size_t zeroes = char_count(data, '0');

    size_t result_len = (ones * 3) + (zeroes * 8);
    char result[result_len + 1];
    result[result_len] = '\0';
    unsigned long buffer_position = 0;

    for (unsigned long i = 0; i < strlen(data); i++) {
        result[buffer_position] = '1';
        result[buffer_position + 1] = '0';
        result[buffer_position + 2] = '0';
        buffer_position += 3;
        if (data[i] == '0') {
            result[buffer_position] = '0';
            result[buffer_position + 1] = '0';
            result[buffer_position + 2] = '0';
            result[buffer_position + 3] = '0';
            result[buffer_position + 4] = '0';
            buffer_position += 5;
        }
    }

    return strdup(result);
}

char *manchester_encode(char *data) {
    size_t data_len = strlen(data);

    char result[data_len * 2 + 1];
    result[data_len * 2] = '\0';

    for (size_t i = 0; i < data_len; i++) {
        if (data[i] == '0') {
            result[i * 2] = '0';
            result[(i * 2) + 1] = '1';
        }
        else {
            result[i * 2] = '1';
            result[(i * 2) + 1] = '0';
        }
    }

    return strdup(result);
}

void sigint_callback_handler(int signum) {
    fprintf(stderr, "Caught signal %d\n", signum);
    g_main_loop_quit(loop);
}

void help(char *name) {
    printf("Usage : %s [OPTIONS]\n", name);
    printf(" -a house_bits        house address associated with your NEXA/Proove/Anslut\n");
    printf(" -g group_bit         communicate to a group (?)\n");
    printf(" -s state_bit         modify the state of a switch [ON = 0 | 1 = OFF]\n");
    printf(" -t type_bits         type of switch [NEXA = 11 | Proove/Anslut = 00]\n");
    printf(" -u unit_bits         unit to communicate with\n");
    printf(" -f hertz             transmit frequency [default: %lu Hz]\n", (long unsigned) transmit_freq);
    printf(" -c hertz             carrier frequency [default: %lu Hz]\n", (long unsigned) carrier_freq);
    printf(" -b us                overall bit duration in microseconds [default: %luµs]\n", (long unsigned) bit_duration);
    printf(" -r repeat_count      repeat message repeat_count times [default: %d times]\n", repeat_count);
    printf(" -w filename          write IQ data to file and exit\n");
    printf(" -h                   show this help\n");
}


int main(int argc, char **argv) {
    int retopt;
    int opt = 0;
    char *endptr;

    if (argc < 2) {
        help(argv[0]);
        return EXIT_FAILURE;
    }
    while ((retopt = getopt(argc, argv, "a:g:s:t:u:f:c:b:w:h")) != -1) {
        switch (retopt) {
            case 'a':
                house_bits = strdup(optarg);
                if (strlen(house_bits) != 26) {
                    printf("house_bits have to be exactly 26 bits long!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'g':
                group_bit = strdup(optarg);
                if (strlen(group_bit) != 1) {
                    printf("group_bit have to be exactly 1 bit long!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 's':
                state_bit = strdup(optarg);
                if (strlen(state_bit) != 1) {
                    printf("state_bit have to be exactly 1 bit long!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 't':
                type_bits = strdup(optarg);
                if (strlen(type_bits) != 2) {
                    printf("type_bits have to be exactly 2 bits long!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'u':
                unit_bits = strdup(optarg);
                if (strlen(unit_bits) != 2) {
                    printf("unit_bits have to be exactly 2 bits long!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'f':
                transmit_freq = (uint64_t) strtoll(optarg, &endptr, 10);
                if (endptr == optarg || transmit_freq == 0) {
                    printf("frequency have to be a valid number!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'c':
                carrier_freq = (uint64_t) strtoll(optarg, &endptr, 10);
                if (endptr == optarg || transmit_freq == 0) {
                    printf("carrier frequency have to be a valid number!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'b':
                bit_duration = (uint64_t) strtoll(optarg, &endptr, 10) * 8;
                if (endptr == optarg || bit_duration == 0) {
                    printf("bit duration have to be a valid number!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'r':
                repeat_count = (int) strtol(optarg, &endptr, 10);
                if (endptr == optarg || repeat_count <= 0) {
                    printf("repeat_count have to be a valid number!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            case 'w':
                filename = strdup(optarg);
                if (strlen(filename) <= 0) {
                    printf("filename have to be valid!\n");
                    return EXIT_FAILURE;
                }
                opt++;
                break;
            default:
                help(argv[0]);
                return EXIT_FAILURE;
        }
    }

    char buffer[33];
    strncpy(buffer, house_bits, sizeof(buffer));
    strncat(buffer, group_bit, sizeof(buffer) - strlen(buffer) - 1);
    strncat(buffer, state_bit, sizeof(buffer) - strlen(buffer) - 1);
    strncat(buffer, type_bits, sizeof(buffer) - strlen(buffer) - 1);
    strncat(buffer, unit_bits, sizeof(buffer) - strlen(buffer) - 1);

    char *prepared_packet = prepare_packet(manchester_encode(buffer));

    char *full_message = malloc((15 + strlen(prepared_packet) + 35) * sizeof(char));
    strcpy(full_message, "100000000000000");
    strncat(full_message, prepared_packet, sizeof(full_message) - strlen(full_message) - 1);
    strncat(full_message, "10000000000000000000000000000000000", sizeof(full_message) - strlen(full_message) - 1);

    unsigned long full_message_length = strlen(full_message);
    message_size = full_message_length * bit_duration;

    tx_buffer_I = malloc(message_size * sizeof(int8_t));
    tx_buffer_Q = malloc(message_size * sizeof(int8_t));

    if (tx_buffer_I == NULL || tx_buffer_Q == NULL) {
        printf("error allocating memory!\n");
        return EXIT_FAILURE;
    }

    memset(tx_buffer_I, 0, message_size * sizeof(int8_t));
    memset(tx_buffer_Q, 0, message_size * sizeof(int8_t));

    int c = 0;
    uint64_t stop;
    uint64_t position;
    double carrier_angle;

    uint64_t full = sample_rate / carrier_freq;

    for (uint64_t i = 0; i < full_message_length; i++) {
        if (full_message[i] == '0') {
            printf("_");
        } else {
            printf("-");
            position = bit_duration * i;
            stop = position + bit_duration;
            while (position < stop) {
                carrier_angle = c * tau / full;
                tx_buffer_I[position] = (int8_t) (127.0 * sin(carrier_angle));
                tx_buffer_Q[position] = (int8_t) (127.0 * cos(carrier_angle));
                position++;
                c++;
                c %= full;
            }
        }
    }

    free(full_message);
    printf("\n");

    if (filename != NULL) {
        printf("Writing to file: %s\n", filename);
        FILE *fd = fopen(filename, "wb");
        for (uint64_t i = 0; i < message_size; i++) {
            fwrite(&tx_buffer_I[i], sizeof(int8_t), 1, fd);
            fwrite(&tx_buffer_Q[i], sizeof(int8_t), 1, fd);
        }
        for (uint64_t i = 0; i < message_size; i++) {
            fwrite(&tx_buffer_I[i], sizeof(int8_t), 1, fd);
            fwrite(&tx_buffer_Q[i], sizeof(int8_t), 1, fd);
        }
        free(tx_buffer_I);
        free(tx_buffer_Q);
        return EXIT_SUCCESS;
    }

    loop = g_main_loop_new(NULL, FALSE);

    signal(SIGINT, &sigint_callback_handler);
    signal(SIGILL, &sigint_callback_handler);
    signal(SIGFPE, &sigint_callback_handler);
    signal(SIGSEGV, &sigint_callback_handler);
    signal(SIGTERM, &sigint_callback_handler);
    signal(SIGABRT, &sigint_callback_handler);

    int result;

    result = hackrf_init();
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_init() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    result = hackrf_open(&device);
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_open() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    result = hackrf_set_sample_rate_manual(device, sample_rate, 1);
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_sample_rate_set() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    result = hackrf_set_baseband_filter_bandwidth(device, hackrf_compute_baseband_filter_bw_round_down_lt(sample_rate));
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_baseband_filter_bandwidth_set() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    result = hackrf_set_txvga_gain(device, gain);
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_set_txvga_gain() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    result = hackrf_set_freq(device, transmit_freq);
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_set_freq() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }


    result = hackrf_set_amp_enable(device, (uint8_t) 1);
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_set_amp_enable() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    result = hackrf_start_tx(device, tx_callback, NULL);
    if (result != HACKRF_SUCCESS) {
        fprintf(stderr, "hackrf_start_tx() failed: %s (%d)\n", hackrf_error_name(result), result);
        return EXIT_FAILURE;
    }

    g_main_loop_run(loop);

    g_main_loop_unref(loop);

    if (device != NULL) {
        result = hackrf_stop_tx(device);
        if (result != HACKRF_SUCCESS) {
            fprintf(stderr, "hackrf_stop_tx() failed: %s (%d)\n", hackrf_error_name(result), result);
        }
        result = hackrf_close(device);
        if (result != HACKRF_SUCCESS) {
            fprintf(stderr, "hackrf_close() failed: %s (%d)\n", hackrf_error_name(result), result);
        }
        hackrf_exit();
    }

    free(tx_buffer_I);
    free(tx_buffer_Q);

    return EXIT_SUCCESS;
}
